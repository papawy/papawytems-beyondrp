# Papawytems
Plugin that manages items (creation is included)
Custom version for Beyond RolePlay uses.

# Short explanation

In your script, you must first create a ItemSchema to be a model for all items you want to create.
You must set a name, a modelID, and a type (it's a simple number).

After, you can create items. Set item positions and rotations, and show it !

# Build
SAMPGDK4 in a dynamic version is required.

Next, you should launch CMake and try to generate sources. After that, compile the plugin with generated files !

# Documentation

Here : https://github.com/Papawy/Papawytems/wiki

# Credits

Modifications of the original plugin inspired by SouthclawInteractiveFramework.
