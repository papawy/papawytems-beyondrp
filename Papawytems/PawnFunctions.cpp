#include "PawnFunctions.h"

namespace ItemNatives {

	ItemManager itmManager;

	float defaultDrawDist = 300.0;

	// PAWN Native : native SetDefaultItemDrawDist(Float:distance) | return : nothing
	cell AMX_NATIVE_CALL SetDefaultItemDrawDist(AMX* amx, cell* params)
	{
		defaultDrawDist = amx_ctof(params[1]);
		return 1;
	}

	// --- ITEMS MANAGER ---

	// PAWN Native : native IsItemExist(itemID) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL IsItemExist(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
			return 1;
		else
			return 0;
	}

	// PAWN Native : native IsItemSchemaExist(itemSchemaID) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL IsItemSchemaExist(AMX* amx, cell* params)
	{
		if (itmManager.itemSchemaExist(params[1]))
			return 1;
		else
			return 0;
	}

	// PAWN Native : native GetMaxItems() | return number of stored items
	cell AMX_NATIVE_CALL GetMaxItems(AMX* amx, cell* params)
	{
		return itmManager.getMaxItems();
	}


	// ---- ITEM SCHEMA ----

	// PAWN Native : native CreateItemSchema(SchemaName[], typeID, modelID, isContainer = false) | return : SchemaID : /!\ Important 
	cell AMX_NATIVE_CALL CreateItemSchema(AMX* amx, cell* params)
	{
		// Creating temporary Schema
		ItemSchema tmpShem(getPawnString(amx, params[1]), params[2], params[3], params[4] != 0);

		/*// Getting string param address
		cell *addr = NULL;
		// SchemaName
		amx_GetAddr(amx, params[1], &addr);

		// First param
		int len = NULL;

		amx_StrLen(addr, &len);

		if (len)
		{
		len++;
		char* SchemaName = new char[len];
		amx_GetString(SchemaName, addr, 0, len);

		tmpShem.setItemName(SchemaName);
		delete[] SchemaName;
		}*/
		/*
		tmpShem.setItemName(getPawnString(amx, params[1]));

		// Second param
		tmpShem.setItemType(params[2]);

		// Third param
		tmpShem.setItemModel(params[3]);
		*/

		// Pushing tmp Schema into ItemManager
		return itmManager.addItemSchema(tmpShem);
	}

	// PAWN Native : native SetItemSchemaName(itemSchemaID, SchemaName[]) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaName(AMX* amx, cell* params)
	{
		if (itmManager.itemSchemaExist(params[1]))
		{
			/*
			// Gettinf new SchemaName str
			cell *addr = NULL;

			amx_GetAddr(amx, params[2], &addr);

			int len = NULL;

			amx_StrLen(addr, &len);

			if (len)
			{
			len++;

			char* newShemName = new char[len];
			amx_GetString(newShemName, addr, 0, len);

			itmManager.accessItemSchema(params[1]).setItemName(newShemName);
			delete[] newShemName;
			}*/

			itmManager.accessItemSchema(params[1]).setItemName(getPawnString(amx, params[2]));

			return 1;
		}
		else
			return 0;

	}

	// PAWN Native : native SetItemSchemaType(itemSchemaID, SchemaType) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaType(AMX* amx, cell* params)
	{
		if (itmManager.itemSchemaExist(params[1]))
		{
			itmManager.accessItemSchema(params[1]).setItemType(params[2]);
			return 1;
		}
		else
			return 0;
	}

	// PAWN Native : native SetItemSchemaSubType(itemSchemaID, SchemaSubType) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaSubType(AMX* amx, cell* params)
	{
		if (itmManager.itemSchemaExist(params[1]))
		{
			itmManager.accessItemSchema(params[1]).setItemSubType(params[2]);
			return 1;
		}
		else
			return 0;
	}

	// PAWN Native : native SetItemSchemaModel(itemSchemaID, SchemaModel) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaModel(AMX* amx, cell* params)
	{
		if (itmManager.itemSchemaExist(params[1]))
		{
			itmManager.accessItemSchema(params[1]).setItemModel(params[2]);
			return 1;
		}
		else
			return 0;
	}

	// PAWN Native : native SetItemSchemaSize(itemSchemaID, itemSize) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaSize(AMX* amx, cell* params)
	{
		if (itmManager.itemSchemaExist(params[1]))
		{
			itmManager.accessItemSchema(params[1]).setItemSize(amx_ctof(params[2]));
			return 1;
		}
		else
			return 0;
	}

	// PAWN Native : native SetItemSchemaStackableState(itemSchemaID, bool:isStackable) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaStackableState(AMX* amx, cell* params)
	{
		if (itmManager.itemSchemaExist(params[1]))
		{
			if (itmManager.accessItemSchema(params[1]).isItemContainer())
			{
				itmManager.accessItemSchema(params[1]).setItemDefaultRemainingUses(false);
			}
			itmManager.accessItemSchema(params[1]).setItemStackableState(params[2] != 0);
			return 1;
		}
		else
			return 0;
	}

	// PAWN Native : native SetItemSchemaStorableState(itemSchemaID, bool:isRamassable) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaStorableState(AMX* amx, cell* params)
	{
		if (itmManager.itemSchemaExist(params[1]))
		{
			itmManager.accessItemSchema(params[1]).setItemStorableState(params[2] != 0);
			return 1;
		}
		else
			return 0;
	}

	// PAWN Native : native SetItemSchemaDefaultRemainingUses(itemSchemaID, remainingUses) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaDefaultRemainingUses(AMX* amx, cell* params)
	{
		if (itmManager.itemSchemaExist(params[1]))
		{
			if (itmManager.accessItemSchema(params[1]).isItemContainer())
			{
				itmManager.accessItemSchema(params[1]).setItemDefaultRemainingUses(-1);
			}
			itmManager.accessItemSchema(params[1]).setItemDefaultRemainingUses(params[2]);
			return 1;
		}
		else
			return 0;
	}

	// ----- ITEMS --------

	// PAWN Native : native CreateItem(SchemaID) | return : itemID : /!\ Important 
	cell AMX_NATIVE_CALL CreateItem(AMX* amx, cell* params)
	{
		// Creating tempo item and assign ItemSchema
		try
		{
			if (itmManager.accessItemSchema(params[1]).isItemContainer())
			{
				ItemContainer tmpContainer(itmManager.accessItemSchema(params[1]), &itmManager);
				tmpContainer.setDrawDistance(defaultDrawDist);
				return itmManager.addItem(&tmpContainer);
			}
			else
			{
				Item tmpItem(itmManager.accessItemSchema(params[1]));
				tmpItem.setDrawDistance(defaultDrawDist);
				return itmManager.addItem(&tmpItem);
			}
		}
		catch (int e)
		{
			return e;
		}
	}

	// PAWN Native : native CreateItemEx(SchemaID, Float:posx, Float:posy, Float:posz, Float:rotx, Float:roty, Float:rotz, bool:draw=true) | return : itemID : /!\ Important 
	cell AMX_NATIVE_CALL CreateItemEx(AMX* amx, cell* params)
	{
		try
		{
			int itemID;
			if (itmManager.accessItemSchema(params[1]).isItemContainer())
			{
				ItemContainer tmpContainer(itmManager.accessItemSchema(params[1]), &itmManager);
				tmpContainer.setPosition(ObjectPosition(
					amx_ctof(params[2]),
					amx_ctof(params[3]),
					amx_ctof(params[4]),
					amx_ctof(params[5]),
					amx_ctof(params[6]),
					amx_ctof(params[7])));
				tmpContainer.setDrawDistance(defaultDrawDist);
				itemID = itmManager.addItem(&tmpContainer);
			}
			else
			{
				Item tmpItem(itmManager.accessItemSchema(params[1]));
				tmpItem.setPosition(ObjectPosition(
					amx_ctof(params[2]),
					amx_ctof(params[3]),
					amx_ctof(params[4]),
					amx_ctof(params[5]),
					amx_ctof(params[6]),
					amx_ctof(params[7])));
				tmpItem.setDrawDistance(defaultDrawDist);
				itemID = itmManager.addItem(&tmpItem);
			}

			if (params[8] == 1)
			{
				itmManager.accessItem(itemID)->Show();
			}

			return itemID;
		}
		catch (int e)
		{
			return e;
		}
	}

	// PAWN Native : native DestroyItem(itemID) | return 0 if item is not found, 1 if item is removed
	cell AMX_NATIVE_CALL DestroyItem(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			itmManager.removeItem(params[1]);
			return 1;
		}
		else
			return 0;
	}




	// PAWN Native : native SetItemPos(itemID, Float:posx, Float:posy, Float:posz) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemPos(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			ObjectPosition tmpPos(itmManager.accessItem(params[1])->getPosition());
			tmpPos.px = amx_ctof(params[2]);
			tmpPos.py = amx_ctof(params[3]);
			tmpPos.pz = amx_ctof(params[4]);

			itmManager.accessItem(params[1])->setPosition(tmpPos);
			return 1;
		}
		else
			return 0;
	}

	// PAWN Native : native GetItemPos(itemID, &Float:posx, &Float:posy, &Float:posz) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL GetItemPos(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			cell* addr[3] = { NULL, NULL, NULL };

			amx_GetAddr(amx, params[2], &addr[0]);
			amx_GetAddr(amx, params[3], &addr[1]);
			amx_GetAddr(amx, params[4], &addr[2]);

			*addr[0] = amx_ftoc(itmManager.accessItem(params[1])->getPosition().px);
			*addr[1] = amx_ftoc(itmManager.accessItem(params[1])->getPosition().py);
			*addr[2] = amx_ftoc(itmManager.accessItem(params[1])->getPosition().pz);

			return 1;
		}
		else
			return 0;
	}


	// PAWN Native : native SetItemRot(itemID, Float:rotx, Float:roty, Float:rotz) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemRot(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			ObjectPosition tmpPos(itmManager.accessItem(params[1])->getPosition());
			tmpPos.rx = amx_ctof(params[2]);
			tmpPos.ry = amx_ctof(params[3]);
			tmpPos.rz = amx_ctof(params[4]);

			itmManager.accessItem(params[1])->setPosition(tmpPos);
			return 1;
		}
		else
			return 0;
	}

	// PAWN Native : native GetItemRot(itemID, &Float:rotx, &Float:roty, &Float:rotz) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL GetItemRot(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			cell* addr[3] = { NULL, NULL, NULL };

			amx_GetAddr(amx, params[2], &addr[0]);
			amx_GetAddr(amx, params[3], &addr[1]);
			amx_GetAddr(amx, params[4], &addr[2]);

			*addr[0] = amx_ftoc(itmManager.accessItem(params[1])->getPosition().rx);
			*addr[1] = amx_ftoc(itmManager.accessItem(params[1])->getPosition().ry);
			*addr[2] = amx_ftoc(itmManager.accessItem(params[1])->getPosition().rz);

			return 1;
		}
		else
			return 0;
	}





	// PAWN Native : native SetItemName(itemID, itemName[]) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemName(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			/*// Gettinf new itemName str
			cell *addr = NULL;

			amx_GetAddr(amx, params[2], &addr);

			int len = NULL;

			amx_StrLen(addr, &len);

			if (len)
			{
			len++;

			char* newItemName = new char[len];
			amx_GetString(newItemName, addr, 0, len);

			itmManager.accessItem(params[1]).setName(newItemName);
			delete[] newItemName;
			}*/

			itmManager.accessItem(params[1])->setName(getPawnString(amx, params[2]));

			return 1;
		}
		else
			return 0;
	}

	// PAWN Native : native SetItemDefaultName(itemID) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemDefaultName(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			itmManager.accessItem(params[1])->setDefaultName();
			return 1;
		}
		else
			return 0;
	}

	// PAWN Native : native GetItemName(itemID, itemName[], len = sizeof(itemName)) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL GetItemName(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			cell *addr = NULL;

			amx_GetAddr(amx, params[2], &addr);
			amx_SetString(addr, itmManager.accessItem(params[1])->getName().c_str(), 0, 0, params[3]);
			return 1;
		}
		else
			return 0;
	}





	// PAWN Native : native SetItemVarInt(itemID, varName[], value) | return 0 if item is not found, 1 if variable is created, 2 if variable is setted, and -1 if wrong type for the varName
	cell AMX_NATIVE_CALL SetItemVarInt(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			// Gettinf varName str
			cell *addr = NULL;

			amx_GetAddr(amx, params[2], &addr);

			int len = NULL;

			amx_StrLen(addr, &len);

			if (len)
			{
				len++;

				char* varName = new char[len];
				amx_GetString(varName, addr, 0, len);

				if (itmManager.accessItem(params[1])->isVarIntExist(varName))
				{
					itmManager.accessItem(params[1])->setVarInt(varName, params[2]);
					return -1;
				}
				else
				{
					itmManager.accessItem(params[1])->createVarInt(varName, params[2]);
					return 2;;
				}
				delete[] varName;
			}
			return 0;
		}
		else
			return 0;
	}

	// PAWN Native : native RemoveItemVarInt(itemID, varName[]) | return 0 if item is not found, 1 if variable was removed, 2 if variable was not found
	cell AMX_NATIVE_CALL RemoveItemVarInt(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			// Gettinf varName str
			cell *addr = NULL;

			amx_GetAddr(amx, params[2], &addr);

			int len = NULL;

			amx_StrLen(addr, &len);

			if (len)
			{
				len++;

				char* varName = new char[len];
				amx_GetString(varName, addr, 0, len);

				if (itmManager.accessItem(params[1])->isVarIntExist(varName))
				{
					itmManager.accessItem(params[1])->removeVarInt(varName);
					return 1;
				}
				else
				{
					return 2;
				}
				delete[] varName;
			}
			return 2;
		}
		else
			return 0;
	}

	// PAWN Native : native GetItemVarInt(itemID, varName[]) | return 0 if item is not found or if variable doesn't exist, variable value if variable was found
	cell AMX_NATIVE_CALL GetItemVarInt(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			// Gettinf varName str
			cell *addr = NULL;

			amx_GetAddr(amx, params[2], &addr);

			int len = NULL;

			amx_StrLen(addr, &len);

			if (len)
			{
				len++;

				char* varName = new char[len];
				amx_GetString(varName, addr, 0, len);

				if (itmManager.accessItem(params[1])->isVarIntExist(varName))
				{
					return itmManager.accessItem(params[1])->getVarInt(varName);
				}
				else
				{
					return 0;
				}
				delete[] varName;
			}
			return 0;
		}
		else
			return 0;
	}


	// PAWN Native : native SetItemVarFloat(itemID, varName[], value) | return 0 if item is not found, 1 if variable is created, 2 if variable is setted, and -1 if wrong type for the varName
	cell AMX_NATIVE_CALL SetItemVarFloat(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			// Gettinf varName str
			cell *addr = NULL;

			amx_GetAddr(amx, params[2], &addr);

			int len = NULL;

			amx_StrLen(addr, &len);

			if (len)
			{
				len++;

				char* varName = new char[len];
				amx_GetString(varName, addr, 0, len);

				if (itmManager.accessItem(params[1])->isVarFloatExist(varName))
				{
					itmManager.accessItem(params[1])->setVarFloat(varName, amx_ctof(params[2]));
					return -1;
				}
				else
				{
					itmManager.accessItem(params[1])->createVarFloat(varName, amx_ctof(params[2]));
					return 2;;
				}
				delete[] varName;
			}
			return 0;
		}
		else
			return 0;
	}

	// PAWN Native : native RemoveItemVarFloat(itemID, varName[]) | return 0 if item is not found, 1 if variable was removed, 2 if variable was not found
	cell AMX_NATIVE_CALL RemoveItemVarFloat(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			// Gettinf varName str
			cell *addr = NULL;

			amx_GetAddr(amx, params[2], &addr);

			int len = NULL;

			amx_StrLen(addr, &len);

			if (len)
			{
				len++;

				char* varName = new char[len];
				amx_GetString(varName, addr, 0, len);

				if (itmManager.accessItem(params[1])->isVarFloatExist(varName))
				{
					itmManager.accessItem(params[1])->removeVarFloat(varName);
					return 1;
				}
				else
				{
					return 2;
				}
				delete[] varName;
			}
			return 2;
		}
		else
			return 0;
	}

	// PAWN Native : native Float:GetItemVarFloat(itemID, varName[]) | return 0 if item is not found or if variable doesn't exist, variable value if variable was found
	cell AMX_NATIVE_CALL GetItemVarFloat(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			// Gettinf varName str
			cell *addr = NULL;

			amx_GetAddr(amx, params[2], &addr);

			int len = NULL;

			amx_StrLen(addr, &len);

			if (len)
			{
				len++;

				char* varName = new char[len];
				amx_GetString(varName, addr, 0, len);

				if (itmManager.accessItem(params[1])->isVarFloatExist(varName))
				{
					const float f = itmManager.accessItem(params[1])->getVarFloat(varName);
					return amx_ftoc(f);
				}
				else
				{
					return 0;
				}
				delete[] varName;
			}
			return 0;
		}
		else
			return 0;
	}


	// PAWN Native : native SetItemVarStr(itemID, varName[], value[]) | return 0 if item is not found, 1 if variable is created, 2 if variable is setted, and -1 if wrong type for the varName
	cell AMX_NATIVE_CALL SetItemVarStr(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			// Gettinf varName str
			cell *addr = NULL;

			amx_GetAddr(amx, params[2], &addr);

			int len = NULL;

			amx_StrLen(addr, &len);

			if (len)
			{
				len++;

				char* varName = new char[len];
				amx_GetString(varName, addr, 0, len);

				if (itmManager.accessItem(params[1])->isVarStrExist(varName))
				{
					itmManager.accessItem(params[1])->setVarStr(varName, getPawnString(amx, params[2]));
					return -1;
				}
				else
				{
					itmManager.accessItem(params[1])->createVarStr(varName, getPawnString(amx, params[2]));
					return 2;;
				}
				delete[] varName;
			}
			return 0;
		}
		else
			return 0;
	}

	// PAWN Native : native RemoveItemVarStr(itemID, varName[]) | return 0 if item is not found, 1 if variable was removed, 2 if variable was not found
	cell AMX_NATIVE_CALL RemoveItemVarStr(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			// Gettinf varName str
			cell *addr = NULL;

			amx_GetAddr(amx, params[2], &addr);

			int len = NULL;

			amx_StrLen(addr, &len);

			if (len)
			{
				len++;

				char* varName = new char[len];
				amx_GetString(varName, addr, 0, len);

				if (itmManager.accessItem(params[1])->isVarStrExist(varName))
				{
					itmManager.accessItem(params[1])->removeVarStr(varName);
					return 1;
				}
				else
				{
					return 2;
				}
				delete[] varName;
			}
			return 2;
		}
		else
			return 0;
	}

	// PAWN Native : native GetItemVarStr(itemID, varName[], value[], len = sizeof(value)) | return 0 if item is not found or if variable doesn't exist, 1 if found and value[] is filled
	cell AMX_NATIVE_CALL GetItemVarStr(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			// Gettinf varName str
			cell *addr = NULL;

			amx_GetAddr(amx, params[2], &addr);

			int len = NULL;

			amx_StrLen(addr, &len);

			if (len)
			{
				len++;

				char* varName = new char[len];
				amx_GetString(varName, addr, 0, len);

				if (itmManager.accessItem(params[1])->isVarStrExist(varName))
				{
					cell* valAddr = NULL;

					amx_GetAddr(amx, params[3], &valAddr);
					amx_SetString(valAddr, itmManager.accessItem(params[1])->getVarStr(varName).c_str(), 0, 0, params[2]);

					return 1;
				}
				else
				{
					return 0;
				}
				delete[] varName;
			}
			return 0;
		}
		else
			return 0;
	}





	// PAWN Native : native Float:GetItemSize(itemID) | return -1 if item is not found, or size otherwise
	cell AMX_NATIVE_CALL GetItemSize(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			const float tmpSize = itmManager.accessItem(params[1])->getItemSize();
			return amx_ftoc(tmpSize);
		}
		else
			return -1;
	}

	// PAWN Native : native bool:IsItemStackable(itemID) | return -1 if item is not found, or bool otherwise
	cell AMX_NATIVE_CALL IsItemStackable(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			return itmManager.accessItem(params[1])->isItemStackable();
		}
		else
			return -1;
	}

	// PAWN Native : native bool:IsItemStorable(itemID) | return -1 if item is not found, or bool otherwise
	cell AMX_NATIVE_CALL IsItemStorable(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			return itmManager.accessItem(params[1])->isItemStorable();
		}
		else
			return -1;
	}

	// PAWN Native : native GetRemainingUses(itemID) | return -1 if item is not found, remaining uses otherwise
	cell AMX_NATIVE_CALL GetRemainingUses(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			return itmManager.accessItem(params[1])->getItemRemainingUses();
		}
		else
			return -1;
	}

	// PAWN Native : native SetRemainingUses(itemID, remainingUses) | return 0 if item is not found, or type otherwise
	cell AMX_NATIVE_CALL SetRemainingUses(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			itmManager.accessItem(params[1])->setItemRemainingUses(params[2]);
			return 1;
		}
		else
			return 0;
	}

	// PAWN Native : native RemoveOneUse(itemID) | return 0 if item is not found, or type otherwise
	cell AMX_NATIVE_CALL RemoveOneUse(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			itmManager.accessItem(params[1])->setItemRemainingUses(itmManager.accessItem(params[1])->getItemRemainingUses() - 1);
			return 1;
		}
		else
			return 0;
	}





	// PAWN Native : native GetItemType(itemID) | return 0 if item is not found, or type otherwise
	cell AMX_NATIVE_CALL GetItemType(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			return itmManager.accessItem(params[1])->getSchema().getItemType();
		}
		else
			return 0;
	}

	// PAWN Native : native GetItemSubType(itemID) | return 0 if item is not found, or subType otherwise
	cell AMX_NATIVE_CALL GetItemSubType(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			return itmManager.accessItem(params[1])->getSchema().getItemSubType();
		}
		else
			return 0;
	}


	// PAWN Native : native GetItemModel(itemID) | return 0 if item is not found, or model id otherwise
	cell AMX_NATIVE_CALL GetItemModel(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			return itmManager.accessItem(params[1])->getSchema().getItemModel();
		}
		else
			return 0;
	}

	// PAWN Native : native GetItemObjectID(itemID) | return 0 if item is not found, or object id otherwise
	cell AMX_NATIVE_CALL GetItemObjectID(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			return itmManager.accessItem(params[1])->getObjectID();
		}
		else
			return 0;
	}





	// PAWN Native : native ShowItem(itemID) | return 0 if item is not found, 2 if it's already shown or 1 otherwise
	cell AMX_NATIVE_CALL ShowItem(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			if (itmManager.accessItem(params[1])->Show())
				return 1;
			else
				return 2;
		}
		else
			return 0;
	}

	// PAWN Native : native HideItem(itemID) | return 0 if item is not found, 2 if it's already hidden or 1 otherwise
	cell AMX_NATIVE_CALL HideItem(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			if (itmManager.accessItem(params[1])->Hide())
				return 1;
			else
				return 2;
		}
		else
			return 0;
	}

	// PAWN Native : native IsItemShown(itemID) | return 0 is item is not found, 1 if it's shown, or 2 ir it's hidden
	cell AMX_NATIVE_CALL IsItemShown(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			if (itmManager.accessItem(params[1])->isShown())
				return 1;
			else
				return 2;
		}
		else
			return 0;
	}

	/*	-- BodyParts --

		head = 0
		eyes = 1
		face = 2

		torso = 3
		back = 4
	
		holster = 5
	*/
	// PAWN native : native SetItemAttachmentPerm(item ID, bodypart, state) | return 0 if item not found or bodypart not found, 1 if bodypart setted
	cell AMX_NATIVE_CALL SetItemAttachmentPerm(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			if ((params[3] != 0) || (params[3] != 1))
				return 0;

			switch (params[2])
			{
			case 0:  itmManager.accessItem(params[1])->getAttachmentsPerms().head = params[3] !=0;
			case 1:  itmManager.accessItem(params[1])->getAttachmentsPerms().eyes = params[3] != 0;
			case 2:  itmManager.accessItem(params[1])->getAttachmentsPerms().face = params[3] != 0;

			case 3:  itmManager.accessItem(params[1])->getAttachmentsPerms().torso = params[3] != 0;
			case 4:  itmManager.accessItem(params[1])->getAttachmentsPerms().back = params[3] != 0;

			case 5:  itmManager.accessItem(params[1])->getAttachmentsPerms().holster = params[3] != 0;

			default: return 0;
			}
		}
		else
			return -1;
	}

	// PAWN native : native GetItemAttachmentPerm(item ID, bodypart) | return -1 if item not found, 0 if not permitted and 1 if permitted
	cell AMX_NATIVE_CALL GetItemAttachmentPerm(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			switch (params[2])
			{
				case 0: return itmManager.accessItem(params[1])->getAttachmentsPerms().head !=0 ;
				case 1: return itmManager.accessItem(params[1])->getAttachmentsPerms().eyes;
				case 2: return itmManager.accessItem(params[1])->getAttachmentsPerms().face;

				case 3: return itmManager.accessItem(params[1])->getAttachmentsPerms().torso;
				case 4: return itmManager.accessItem(params[1])->getAttachmentsPerms().back;

				case 5: return itmManager.accessItem(params[1])->getAttachmentsPerms().holster;

				default: return 0;
			}
		}
		else
			return -1;
	}


	// PAWN Native : native SetItemDrawDistance(itemID, Float:distance) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemDrawDistance(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			itmManager.accessItem(params[1])->setDrawDistance(amx_ctof(params[2]));
			return 1;
		}
		else
			return 0;
	}

	// PAWN Native : native GetItemSchemaID(itemID) | return -1 if item is not found or itemSchema id if it was found
	cell AMX_NATIVE_CALL GetItemSchemaID(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			return itmManager.getItemSchemaID(params[1]);
		}
		else
			return -1;
	}


	// PAWN Native : native GetItemByObjectID(objectID) | return -1 if not found, or item ID if found
	cell AMX_NATIVE_CALL GetItemByObjectID(AMX* amx, cell* params)
	{
		return itmManager.getItemByObjectID(params[1]);
	}



	// PAWN Native : native GetItems( &index) | return -1 if no items, itemID otherwise : the index is the same as strtok and etc.
	cell AMX_NATIVE_CALL GetItems(AMX* amx, cell* params)
	{
		// "C" part
		cell* addr = NULL;
		amx_GetAddr(amx, params[1], &addr);
		
		return itmManager.getItems(*addr);
		return -1;
	}


	// ----- CONTAINER --------

	// PAWN Native : native IsItemContainer(itemID) | return 0 if not, 1 if it is
	cell AMX_NATIVE_CALL IsItemContainer(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			if (itmManager.accessItem(params[1])->getSchema().isItemContainer())
			{
				return 1;
			}
		}
		return 0;
	}

	// PAWN Native : native AddItemInContainer(containerID, itemID) | return 0 if item couldn't be added, 1 otherwise
	cell AMX_NATIVE_CALL AddItemInContainer(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]) && itmManager.itemExist(params[2]))
		{
			if (itmManager.accessItem(params[1])->getSchema().isItemContainer())
			{
				if (dynamic_cast<ItemContainer*>(itmManager.accessItem(params[1])))
				{
					ItemContainer *itmContainer = dynamic_cast<ItemContainer*>(itmManager.accessItem(params[1]));
					return itmContainer->addItem(params[2]);
				}
			}
		}
		return 0;
	}

	/// PAWN Native : native RemoveItemFromContainer(containerID, itemID) | return 0 if couldn't be removed, 1 otherwise
	cell AMX_NATIVE_CALL RemoveItemFromContainer(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]) && itmManager.itemExist(params[2]))
		{
			if (itmManager.accessItem(params[1])->getSchema().isItemContainer())
			{
				if (dynamic_cast<ItemContainer*>(itmManager.accessItem(params[1])))
				{
					ItemContainer *itmContainer = dynamic_cast<ItemContainer*>(itmManager.accessItem(params[1]));
					return itmContainer->removeItem(params[2]);
				}
			}
		}
		return 0;
	}

	// PAWN Native : native IsItemStoredInContainer(containerID, itemID) | return 0 if not stored in, 1 otherwise
	cell AMX_NATIVE_CALL IsItemStoredInContainer(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]) && itmManager.itemExist(params[2]))
		{
			if (itmManager.accessItem(params[1])->getSchema().isItemContainer())
			{
				if (dynamic_cast<ItemContainer*>(itmManager.accessItem(params[1])))
				{
					ItemContainer *itmContainer = dynamic_cast<ItemContainer*>(itmManager.accessItem(params[1]));
					return itmContainer->isItemStoredIn(params[2]);
				}
			}
		}
		return 0;
	}

	// PAWN Native : native GetItemsInContainer(containerID, &index) | return -1 if no items, itemID otherwise : the index is the same as strtok and etc.
	// Index will be -1 when all items were shawn
	cell AMX_NATIVE_CALL GetItemsInContainer(AMX* amx, cell* params)
	{
		if (itmManager.itemExist(params[1]))
		{
			if (itmManager.accessItem(params[1])->getSchema().isItemContainer())
			{
				// "C" part
				cell* addr = NULL;
				amx_GetAddr(amx, params[2], &addr);


				ItemContainer *itmContainer = dynamic_cast<ItemContainer*>(itmManager.accessItem(params[1]));
				return itmContainer->getItemsIn(*addr);
			}
		}
		return -1;
	}
}
