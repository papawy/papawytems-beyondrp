#include <sampgdk/a_objects.h>
#include <sampgdk/a_samp.h>
#include <sampgdk/core.h>
#include <sampgdk/sdk.h>

#include "PawnFunctions.h"

extern void *pAMXFunctions;

using sampgdk::logprintf;


PLUGIN_EXPORT unsigned int PLUGIN_CALL Supports()
{
	return sampgdk::Supports() | SUPPORTS_AMX_NATIVES;
}

PLUGIN_EXPORT bool PLUGIN_CALL Load(void **ppData)
{
	pAMXFunctions = ppData[PLUGIN_DATA_AMX_EXPORTS];
	//logprintf = (logprintf_t)ppData[PLUGIN_DATA_LOGPRINTF];

	logprintf("*******************************\r\n");
	logprintf(" Papawytems loaded !\r\n");
	logprintf("Compiled on "__DATE__" at "__TIME__"\r\n");
	logprintf("*******************************");
	return sampgdk::Load(ppData);
}

PLUGIN_EXPORT void PLUGIN_CALL Unload()
{
	logprintf("*******************************\r\n");
	logprintf(" Papawytems unloaded !\r\n");
	logprintf("Compiled on "__DATE__" at "__TIME__"\r\n");
	logprintf("*******************************");

	sampgdk::Unload();
}

extern "C" {
	AMX_NATIVE_INFO ItemsNatives[] =
	{
		{ "SetDefaultItemDrawDist", ItemNatives::SetDefaultItemDrawDist },
		// Item Manager
		{ "IsItemExist", ItemNatives::IsItemExist },
		{ "IsItemSchemaExist", ItemNatives::IsItemSchemaExist },
		{ "GetMaxItems", ItemNatives::GetMaxItems },
		// Item Schema
		//		Creation
		{ "CreateItemSchema", ItemNatives::CreateItemSchema },
		//		Properties
		{ "SetItemSchemaName", ItemNatives::SetItemSchemaName },
		{ "SetItemSchemaType", ItemNatives::SetItemSchemaType },
		{ "SetItemSchemaSubType", ItemNatives::SetItemSchemaSubType },
		{ "SetItemSchemaModel", ItemNatives::SetItemSchemaModel },
		{ "SetItemSchemaSize", ItemNatives::SetItemSchemaSize },
		{ "SetItemSchemaStackableState", ItemNatives::SetItemSchemaStackableState },
		{ "SetItemSchemaStorableState", ItemNatives::SetItemSchemaStorableState },
		{ "SetItemSchemaRemainingUses", ItemNatives::SetItemSchemaDefaultRemainingUses },
		// Items
		//		Creation
		{ "CreateItem", ItemNatives::CreateItem },
		{ "CreateItemEx", ItemNatives::CreateItemEx },
		{ "DestroyItem", ItemNatives::DestroyItem },
		//		Positions
		{ "SetItemPos", ItemNatives::SetItemPos },
		{ "GetItemPos", ItemNatives::GetItemPos },
		{ "SetItemRot", ItemNatives::SetItemRot },
		{ "GetItemRot", ItemNatives::GetItemRot },
		//		Name
		{ "SetItemName", ItemNatives::SetItemName },
		{ "SetItemDefaultName", ItemNatives::SetItemDefaultName },
		{ "GetItemName", ItemNatives::GetItemName },
		//		Vars
		{ "SetItemVarInt", ItemNatives::SetItemVarInt },
		{ "RemoveItemVarInt", ItemNatives::RemoveItemVarInt },
		{ "GetItemVarInt", ItemNatives::GetItemVarInt },

		{ "SetItemVarFloat", ItemNatives::SetItemVarFloat },
		{ "RemoveItemVarFloat", ItemNatives::RemoveItemVarFloat },
		{ "GetItemVarFloat", ItemNatives::GetItemVarFloat },

		{ "SetItemVarStr", ItemNatives::SetItemVarStr },
		{ "RemoveItemVarStr", ItemNatives::RemoveItemVarStr },
		{ "GetItemVarStr", ItemNatives::GetItemVarStr },
		//		Property
		{ "GetItemSize", ItemNatives::GetItemSize },
		{ "IsItemStackable", ItemNatives::IsItemStackable },
		{ "IsItemStorable", ItemNatives::IsItemStorable },
		{ "GetRemainingUses", ItemNatives::GetRemainingUses },
		{ "SetRemainingUses", ItemNatives::SetRemainingUses },
		{ "RemoveOneUse", ItemNatives::RemoveOneUse	},
		//		Schema accessors
		{ "GetItemType", ItemNatives::GetItemType },
		{ "GetItemSubType", ItemNatives::GetItemSubType },
		{ "GetItemModel", ItemNatives::GetItemModel },
		{ "GetItemSchemaID", ItemNatives::GetItemSchemaID },
		//		Drawing
		{ "GetItemObjectID", ItemNatives::GetItemObjectID },
		{ "ShowItem", ItemNatives::ShowItem },
		{ "HideItem", ItemNatives::HideItem },
		{ "IsItemShown", ItemNatives::IsItemShown },
		//		Attachments Permissions
		{ "SetItemAttachmentPerm", ItemNatives::SetItemAttachmentPerm },
		{ "GetItemAttachmentPerm", ItemNatives::GetItemAttachmentPerm },
		//		Others
		{ "SetItemDefaultName", ItemNatives::SetItemDefaultName },
		{ "GetItemByObjectID", ItemNatives::GetItemByObjectID },
		{ "GetItems", ItemNatives::GetItems },
		// Containers
		//
		{ "IsItemContainer", ItemNatives::IsItemContainer },
		{ "AddItemInContainer", ItemNatives::AddItemInContainer },
		{ "RemoveItemFromContainer", ItemNatives::RemoveItemFromContainer },
		{ "IsItemStoredInContainer", ItemNatives::IsItemStoredInContainer },
		{ "GetItemsInContainer", ItemNatives::GetItemsInContainer },
		{ 0, 0 }
	};
}

PLUGIN_EXPORT int PLUGIN_CALL AmxLoad(AMX *amx)
{
	return amx_Register(amx, ItemsNatives, -1);
}


PLUGIN_EXPORT int PLUGIN_CALL AmxUnload(AMX *amx)
{
	return AMX_ERR_NONE;
}