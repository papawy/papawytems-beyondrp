#ifndef ITEM_H_INCLUDED
#define ITEM_H_INCLUDED

#include <map>

#include <sampgdk/a_objects.h>

#include "Utils.h"

#include "ItemSchema.h"

#include "AttachmentPerms.h"

class Item
{
public:
	Item();
	Item(ItemSchema& Schema);
	Item(ItemSchema& Schema, ObjectPosition position);

	Item(const Item& other);

	virtual ~Item();

	// Setters

	void setAttachmentsPermissions(AttachmentPerms& attachPerms) { m_attachPerms = attachPerms; };

	void setPosition(float posX, float posY, float posZ);
	void setPosition(ObjectPosition position);

	void setRotation(float rotX, float rotY, float rotZ);

	void setName(std::string name);

	bool setVarInt(std::string varName, int itmVar);
	bool setVarFloat(std::string varName, float itmVar);
	bool setVarStr(std::string varName, std::string itmVar);

	void setDefaultName();

	void setDrawDistance(float dist);

	void setItemStorableState(bool storable) { m_isStorable = storable; };
	void setItemRemainingUses(int remainingUses) { if (remainingUses < 0) { m_remainingUses = remainingUses; } };

	void setContainerState(bool state) { m_isInContainer = state; }; // search an other solution

	// Accessors

	ObjectPosition& getPosition();

	AttachmentPerms& getAttachmentsPerms() { return m_attachPerms; };

	int getVarInt(std::string varName);
	float getVarFloat(std::string varName);
	std::string getVarStr(std::string varName);

	std::string getName();

	ItemSchema& getSchema();

	int getObjectID();

	float getItemSize() { return m_size; };
	virtual bool isItemStackable() { return m_isStackable; };
	bool isItemStorable() { return m_isStorable; };
	int getItemRemainingUses() { return m_remainingUses; };

	bool isInContainer() { return m_isInContainer; };

	// --

	bool Show();
	bool Hide();

	bool createVarInt(std::string varName, int itmVar);
	bool removeVarInt(std::string varName);

	bool createVarFloat(std::string varName, float itmVar);
	bool removeVarFloat(std::string varName);

	bool createVarStr(std::string varName, std::string itmVar);
	bool removeVarStr(std::string varName);

	bool isVarIntExist(std::string varName);
	bool isVarFloatExist(std::string varName);
	bool isVarStrExist(std::string varName);

	bool isShown();

protected:


	ItemSchema *m_schema;

	std::string m_name;

	AttachmentPerms m_attachPerms;

	// Variables system need to be upgraded but no time

	std::map < std::string, int > m_varsInt;

	std::map < std::string, float > m_varsFloat;

	std::map < std::string, std::string > m_varsStr;
	
	ObjectPosition m_position;

	bool m_shown;

	int m_gtaObjectId;

	float m_drawDist;

	float m_size;
	bool m_isStackable;
	bool m_isStorable;
	int m_remainingUses;

	bool m_isInContainer;
};
#endif // ITEM_H_INCLUDED