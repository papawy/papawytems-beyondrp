#ifndef PAWN_FUNCTIONS_H_INCLUDED
#define PAWN_FUNCTIONS_H_INCLUDED


#include <sampgdk/a_objects.h>
#include <sampgdk/a_samp.h>
#include <sampgdk/core.h>
#include <sampgdk/sdk.h>

#include "ItemManager.h"


namespace ItemNatives {

	// PAWN Native : native SetDefaultItemDrawDist(Float:distance) | return : nothing
	cell AMX_NATIVE_CALL SetDefaultItemDrawDist(AMX* amx, cell* params);

	// --- ITEMS MANAGER ---

	// PAWN Native : native IsItemExist(itemID) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL IsItemExist(AMX* amx, cell* params);

	// PAWN Native : native IsItemSchemaExist(itemSchemaID) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL IsItemSchemaExist(AMX* amx, cell* params);

	// PAWN Native : native GetMaxItems() | return number of stored items
	cell AMX_NATIVE_CALL GetMaxItems(AMX* amx, cell* params);


	// ---- ITEM SCHEMA ----

	// PAWN Native : native CreateItemSchema(SchemaName[], typeID, modelID, isContainer = false) | return : SchemaID : /!\ Important 
	cell AMX_NATIVE_CALL CreateItemSchema(AMX* amx, cell* params);

	// PAWN Native : native SetItemSchemaName(itemSchemaID, SchemaName[]) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaName(AMX* amx, cell* params);

	// PAWN Native : native SetItemSchemaType(itemSchemaID, SchemaType) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaType(AMX* amx, cell* params);

	// PAWN Native : native SetItemSchemaSubType(itemSchemaID, SchemaSubType) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaSubType(AMX* amx, cell* params);

	// PAWN Native : native SetItemSchemaModel(itemSchemaID, SchemaModel) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaModel(AMX* amx, cell* params);

	// PAWN Native : native SetItemSchemaSize(itemSchemaID, itemSize) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaSize(AMX* amx, cell* params);

	// PAWN Native : native SetItemSchemaStackableState(itemSchemaID, bool:isStackable) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaStackableState(AMX* amx, cell* params);

	// PAWN Native : native SetItemSchemaStorableState(itemSchemaID, bool:isRamassable) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaStorableState(AMX* amx, cell* params);

	// PAWN Native : native SetItemSchemaDefaultRemainingUses(itemSchemaID, remainingUses) | return 0 if itemSchema is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemSchemaDefaultRemainingUses(AMX* amx, cell* params);

	// ----- ITEMS --------

	// PAWN Native : native CreateItem(SchemaID) | return : itemID : /!\ Important 
	cell AMX_NATIVE_CALL CreateItem(AMX* amx, cell* params);

	// PAWN Native : native CreateItemEx(SchemaID, Float:posx, Float:posy, Float:posz, Float:rotx, Float:roty, Float:rotz, bool:draw=true) | return : itemID : /!\ Important 
	cell AMX_NATIVE_CALL CreateItemEx(AMX* amx, cell* params);

	// PAWN Native : native DestroyItem(itemID) | return 0 if item is not found, 1 if item is removed
	cell AMX_NATIVE_CALL DestroyItem(AMX* amx, cell* params);




	// PAWN Native : native SetItemPos(itemID, Float:posx, Float:posy, Float:posz) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemPos(AMX* amx, cell* params);

	// PAWN Native : native GetItemPos(itemID, &Float:posx, &Float:posy, &Float:posz) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL GetItemPos(AMX* amx, cell* params);


	// PAWN Native : native SetItemRot(itemID, Float:rotx, Float:roty, Float:rotz) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemRot(AMX* amx, cell* params);

	// PAWN Native : native GetItemRot(itemID, &Float:rotx, &Float:roty, &Float:rotz) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL GetItemRot(AMX* amx, cell* params);





	// PAWN Native : native SetItemName(itemID, itemName[]) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemName(AMX* amx, cell* params);

	// PAWN Native : native SetItemDefaultName(itemID) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemDefaultName(AMX* amx, cell* params);

	// PAWN Native : native GetItemName(itemID, itemName[], len = sizeof(itemName)) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL GetItemName(AMX* amx, cell* params);





	// PAWN Native : native SetItemVarInt(itemID, varName[], value) | return 0 if item is not found, 1 if variable is created, 2 if variable is setted, and -1 if wrong type for the varName
	cell AMX_NATIVE_CALL SetItemVarInt(AMX* amx, cell* params);

	// PAWN Native : native RemoveItemVarInt(itemID, varName[]) | return 0 if item is not found, 1 if variable was removed, 2 if variable was not found
	cell AMX_NATIVE_CALL RemoveItemVarInt(AMX* amx, cell* params);

	// PAWN Native : native GetItemVarInt(itemID, varName[]) | return 0 if item is not found or if variable doesn't exist, variable value if variable was found
	cell AMX_NATIVE_CALL GetItemVarInt(AMX* amx, cell* params);


	// PAWN Native : native SetItemVarFloat(itemID, varName[], value) | return 0 if item is not found, 1 if variable is created, 2 if variable is setted, and -1 if wrong type for the varName
	cell AMX_NATIVE_CALL SetItemVarFloat(AMX* amx, cell* params);

	// PAWN Native : native RemoveItemVarFloat(itemID, varName[]) | return 0 if item is not found, 1 if variable was removed, 2 if variable was not found
	cell AMX_NATIVE_CALL RemoveItemVarFloat(AMX* amx, cell* params);

	// PAWN Native : native Float:GetItemVarFloat(itemID, varName[]) | return 0 if item is not found or if variable doesn't exist, variable value if variable was found
	cell AMX_NATIVE_CALL GetItemVarFloat(AMX* amx, cell* params);


	// PAWN Native : native SetItemVarStr(itemID, varName[], value[]) | return 0 if item is not found, 1 if variable is created, 2 if variable is setted, and -1 if wrong type for the varName
	cell AMX_NATIVE_CALL SetItemVarStr(AMX* amx, cell* params);

	// PAWN Native : native RemoveItemVarStr(itemID, varName[]) | return 0 if item is not found, 1 if variable was removed, 2 if variable was not found
	cell AMX_NATIVE_CALL RemoveItemVarStr(AMX* amx, cell* params);

	// PAWN Native : native GetItemVarStr(itemID, varName[], value[], len = sizeof(value)) | return 0 if item is not found or if variable doesn't exist, 1 if found and value[] is filled
	cell AMX_NATIVE_CALL GetItemVarStr(AMX* amx, cell* params);





	// PAWN Native : native Float:GetItemSize(itemID) | return -1 if item is not found, or size otherwise
	cell AMX_NATIVE_CALL GetItemSize(AMX* amx, cell* params);

	// PAWN Native : native bool:IsItemStackable(itemID) | return -1 if item is not found, or bool otherwise
	cell AMX_NATIVE_CALL IsItemStackable(AMX* amx, cell* params);

	// PAWN Native : native bool:IsItemStorable(itemID) | return -1 if item is not found, or bool otherwise
	cell AMX_NATIVE_CALL IsItemStorable(AMX* amx, cell* params);

	// PAWN Native : native GetRemainingUses(itemID) | return -1 if item is not found, remaining uses otherwise
	cell AMX_NATIVE_CALL GetRemainingUses(AMX* amx, cell* params);

	// PAWN Native : native SetRemainingUses(itemID, remainingUses) | return 0 if item is not found, or type otherwise
	cell AMX_NATIVE_CALL SetRemainingUses(AMX* amx, cell* params);
	
	// PAWN Native : native RemoveOneUse(itemID) | return 0 if item is not found, or type otherwise
	cell AMX_NATIVE_CALL RemoveOneUse(AMX* amx, cell* params);





	// PAWN Native : native GetItemType(itemID) | return 0 if item is not found, or type otherwise
	cell AMX_NATIVE_CALL GetItemType(AMX* amx, cell* params);

	// PAWN Native : native GetItemSubType(itemID) | return 0 if item is not found, or subType otherwise
	cell AMX_NATIVE_CALL GetItemSubType(AMX* amx, cell* params);


	// PAWN Native : native GetItemModel(itemID) | return 0 if item is not found, or model id otherwise
	cell AMX_NATIVE_CALL GetItemModel(AMX* amx, cell* params);

	// PAWN Native : native GetItemObjectID(itemID) | return 0 if item is not found, or object id otherwise
	cell AMX_NATIVE_CALL GetItemObjectID(AMX* amx, cell* params);





	// PAWN Native : native ShowItem(itemID) | return 0 if item is not found, 2 if it's already shown or 1 otherwise
	cell AMX_NATIVE_CALL ShowItem(AMX* amx, cell* params);

	// PAWN Native : native HideItem(itemID) | return 0 if item is not found, 2 if it's already hidden or 1 otherwise
	cell AMX_NATIVE_CALL HideItem(AMX* amx, cell* params);

	// PAWN Native : native IsItemShown(itemID) | return 0 is item is not found, 1 if it's shown, or 2 ir it's hidden
	cell AMX_NATIVE_CALL IsItemShown(AMX* amx, cell* params);



	// PAWN native : native SetItemAttachmentPerm(item ID, bodypart, state) | return -1 if item not found, 0 if not permitted and 1 if permitted
	cell AMX_NATIVE_CALL SetItemAttachmentPerm(AMX* amx, cell* params);

	// PAWN native : native GetItemAttachmentPerm(item ID, bodypart) | return -1 if item not found, 0 if not permitted and 1 if permitted
	cell AMX_NATIVE_CALL GetItemAttachmentPerm(AMX* amx, cell* params);



	// PAWN Native : native SetItemDrawDistance(itemID, Float:distance) | return 0 if item is not found, or 1 otherwise
	cell AMX_NATIVE_CALL SetItemDrawDistance(AMX* amx, cell* params);

	// PAWN Native : native GetItemSchemaID(itemID) | return -1 if item is not found or itemSchema id if it was found
	cell AMX_NATIVE_CALL GetItemSchemaID(AMX* amx, cell* params);


	// PAWN Native : native GetItemByObjectID(objectID) | return -1 if not found, or item ID if found
	cell AMX_NATIVE_CALL GetItemByObjectID(AMX* amx, cell* params);


	// PAWN Native : native GetItems( &index) | return -1 if no items, itemID otherwise : the index is the same as strtok and etc.
	cell AMX_NATIVE_CALL GetItems(AMX* amx, cell* params);



	// ----- CONTAINER --------

	// PAWN Native : native IsItemContainer(itemID) | return 0 if not, 1 if it is
	cell AMX_NATIVE_CALL IsItemContainer(AMX* amx, cell* params);

	// PAWN Native : native AddItemInContainer(containerID, itemID) | return 0 if item couldn't be added, 1 otherwise
	cell AMX_NATIVE_CALL AddItemInContainer(AMX* amx, cell* params);

	// PAWN Native : native RemoveItemFromContainer(containerID, itemID) | return 0 if couldn't be removed, 1 otherwise
	cell AMX_NATIVE_CALL RemoveItemFromContainer(AMX* amx, cell* params);

	// PAWN Native : native IsItemStoredInContainer(containerID, itemID) | return 0 if not stored in, 1 otherwise
	cell AMX_NATIVE_CALL IsItemStoredInContainer(AMX* amx, cell* params);

	// PAWN Native : native GetItemsInContainer(containerID, &index) | return -1 if no items, itemID otherwise : the index is the same as strtok and etc.
	cell AMX_NATIVE_CALL GetItemsInContainer(AMX* amx, cell* params);

}


#endif