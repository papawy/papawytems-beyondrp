#ifndef ITEM_Schema_H_INCLUDED
#define ITEM_Schema_H_INCLUDED

#include <string>

class ItemSchema
{
public:
	ItemSchema();
	ItemSchema(std::string itemName, int itemType, int modelId, bool isContainer = false);

	// Setters

	void setItemName(std::string itemName);

	void setItemType(int type);
	void setItemSubType(int subType);

	void setItemModel(int modelId);

	// -- Item properties

	void setItemSize(float size) { m_size = size; };
	void setItemStackableState(bool stackable) { m_isStackable = stackable; };
	void setItemStorableState(bool storable) { m_isStorable = storable; };
	void setItemDefaultRemainingUses(int remainingUses) { m_remainingUses = remainingUses; };

	// Accessors

	std::string getItemName() const { return m_itemName; };

	int getItemType() const { return m_itemType; };
	int getItemSubType() const { return m_itemSubType; };

	int getItemModel() const { return m_modelID; };

	bool isItemContainer() const { return m_isContainer; };

	// -- Item properties

	float getItemSize() { return m_size; };
	bool isItemStackable() { return m_isStackable; };
	bool isItemStorable() { return m_isStorable; };
	int getDefaultRemainingUses() { return m_remainingUses; };
	// ------
	~ItemSchema();

private:
	int m_itemType;
	int m_itemSubType;

	int m_modelID;

	float m_size;
	bool m_isStackable;
	bool m_isStorable;
	int m_remainingUses;

	bool m_isContainer;

	std::string m_itemName;
};

bool operator==(const ItemSchema& right, const ItemSchema& left);

#endif // ITEM_Schema_H_INCLUDED