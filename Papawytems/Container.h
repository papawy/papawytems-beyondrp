#ifndef CONTAINER_H_INCLUDED
#define CONTAINER_H_INCLUDED

#include <list>

#include "ItemManager.h"
#include "ItemSchema.h"
#include "Item.h"

class ItemManager;

class ItemContainer : public Item
{
	public:
		ItemContainer(ItemManager* itmManagerPtr = nullptr);
		ItemContainer(ItemSchema& Schema, ItemManager* itmManagerPtr = nullptr);
		ItemContainer(ItemSchema& Schema, ObjectPosition position, ItemManager* itmManagerPtr = nullptr);


		virtual ~ItemContainer();

		// --

		void linkToItemManager(ItemManager& itmManager);

		// return code :
		/*
			* 1 : item added
			* 0 : container size reached
			* -1 : ERROR - item already stored in
			* -2 : ERROR - item already in a container
			* -3 : ERROR - container not linker to an ItmManager
		*/
		int addItem(int itemID);

		// return code :
		/*
		* 1 : item removed
		* 0 : ERROR - item not stored in
		* -2 : ERROR - container not linker to an ItmManager
		*/
		int removeItem(int itemID);

		// --

		bool isItemStoredIn(int itemID);

		// --

		int getItemsIn(int &index);

		bool isItemStackable() { return false; };


	protected:

		float m_currentCapacity; // addition of all items sizes in the container

		ItemManager* m_itmManagerLink;
		std::list<int>	m_container;
};


#endif