#ifndef ATTACHPERMS_H_INCLUDED
#define ATTACHPERMS_H_INCLUDED

struct AttachmentPerms
{
	bool head;
	bool eyes;
	bool face;

	bool torso;
	bool back;
	
	bool holster;
	AttachmentPerms() { head = false; eyes = false; face = false; torso = false; back = false; holster = false; };
	AttachmentPerms(bool head, bool eyes, bool face, bool torso, bool back, bool holster) : head(head), eyes(eyes), face(face), torso(torso), back(back), holster(holster){};
};

#endif