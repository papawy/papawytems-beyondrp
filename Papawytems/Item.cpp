#include "Item.h"

// -- Constructors

Item::Item()
{
	m_schema = nullptr;

	m_size = 0.0;
	m_isStackable = false;
	m_isStorable = false;
	m_remainingUses = 1;

	m_isInContainer = false;
}

Item::Item(ItemSchema& Schema)
{
	m_schema = &Schema;

	m_name = m_schema->getItemName();

	m_size = m_schema->getItemSize();
	m_isStackable = m_schema->isItemStackable();
	m_isStorable = m_schema->isItemStorable();
	m_remainingUses = m_schema->getDefaultRemainingUses();

	m_isInContainer = false;
}

Item::Item(ItemSchema& Schema, ObjectPosition position)
{
	m_schema = &Schema;
	m_position = position;

	m_name = m_schema->getItemName();

	m_size = m_schema->getItemSize();
	m_isStackable = m_schema->isItemStackable();
	m_isStorable = m_schema->isItemStorable();
	m_remainingUses = m_schema->getDefaultRemainingUses();

	m_isInContainer = false;
}

Item::Item(const Item& other)
{
	m_schema = other.m_schema;

	m_position = other.m_position;

	m_name = m_schema->getItemName();

	m_size = m_schema->getItemSize();
	m_isStackable = m_schema->isItemStackable();
	m_isStorable = m_schema->isItemStorable();
	m_remainingUses = m_schema->getDefaultRemainingUses();

	m_isInContainer = other.m_isInContainer;
}

// -- Methods

// Setters

void Item::setPosition(float posx, float posy, float posz)
{
	m_position.px = posx;
	m_position.py = posy;
	m_position.pz = posz;
}

void Item::setPosition(ObjectPosition position)
{
	m_position = position;
}

void Item::setRotation(float rotx, float roty, float rotz)
{
	m_position.rx = rotx;
	m_position.ry = roty;
	m_position.rz = rotz;
}

void Item::setName(std::string name)
{
	m_name = name;
}

// --

bool Item::setVarInt(std::string varName, int itmVar)
{
	std::map<std::string, int >::iterator it = m_varsInt.find(varName);
	if (it != m_varsInt.end())
	{
		m_varsInt[varName] = itmVar;
		return true;
	}
	else
	{
		return false;
	}
}

bool Item::setVarFloat(std::string varName, float itmVar)
{
	std::map<std::string, float >::iterator it = m_varsFloat.find(varName);
	if (it != m_varsFloat.end())
	{
		m_varsFloat[varName] = itmVar;
		return true;
	}
	else
	{
		return false;
	}
}

bool Item::setVarStr(std::string varName, std::string itmVar)
{
	std::map<std::string, std::string >::iterator it = m_varsStr.find(varName);
	if (it != m_varsStr.end())
	{
		m_varsStr[varName] = itmVar;
		return true;
	}
	else
	{
		return false;
	}
}

// --

void Item::setDefaultName()
{
	m_name = m_schema->getItemName();
}

void Item::setDrawDistance(float dist)
{
	m_drawDist = dist;
}

// Accessors

ObjectPosition& Item::getPosition()
{
	return m_position;
}

// --

int Item::getVarInt(std::string varName)
{
	std::map<std::string, int >::iterator it = m_varsInt.find(varName);
	if (it != m_varsInt.end())
	{
		return m_varsInt[varName];
	}
	else
	{
		throw - 1;
	}
}

float Item::getVarFloat(std::string varName)
{
	std::map<std::string, float >::iterator it = m_varsFloat.find(varName);
	if (it != m_varsFloat.end())
	{
		return m_varsFloat[varName];
	}
	else
	{
		throw - 1;
	}
}

std::string Item::getVarStr(std::string varName)
{
	std::map<std::string, std::string >::iterator it = m_varsStr.find(varName);
	if (it != m_varsStr.end())
	{
		return m_varsStr[varName];
	}
	else
	{
		throw - 1;
	}
}

// --

std::string Item::getName()
{
	if (m_name.size() < 0)
		return m_schema->getItemName();
	else
		return m_name;
}

ItemSchema& Item::getSchema()
{
	return *m_schema;
}

int Item::getObjectID()
{
	return m_gtaObjectId;
}

// --

bool Item::Show()
{
	if (m_shown != true)
	{
		m_gtaObjectId = CreateObject(m_schema->getItemModel(), m_position.px, m_position.py, m_position.pz, m_position.rx, m_position.ry, m_position.rz, m_drawDist);
		m_shown = true;
		return true;
	}
	else
		return false;

}

bool Item::Hide()
{
	if (m_shown != false)
	{
		DestroyObject(m_gtaObjectId);
		m_gtaObjectId = -1;
		m_shown = false;
		return true;
	}
	else
		return true;

}

bool Item::createVarInt(std::string varName, int itmVar)
{
	std::map<std::string, int >::iterator it = m_varsInt.find(varName);
	if (it == m_varsInt.end())
	{
		setVarInt(varName, itmVar);
		return true;
	}
	else
	{
		return false;
	}
}

bool Item::removeVarInt(std::string varName)
{
	std::map<std::string, int >::iterator it = m_varsInt.find(varName);
	if (it != m_varsInt.end())
	{
		m_varsInt.erase(varName);
		return true;
	}
	else
	{
		return false;
	}
}

bool Item::isVarIntExist(std::string varName)
{
	std::map<std::string, int >::iterator it = m_varsInt.find(varName);
	if (it != m_varsInt.end())
		return true;
	else
		return false;
}

bool Item::createVarFloat(std::string varName, float itmVar)
{
	std::map<std::string, float >::iterator it = m_varsFloat.find(varName);
	if (it == m_varsFloat.end())
	{
		setVarFloat(varName, itmVar);
		return true;
	}
	else
	{
		return false;
	}
}

bool Item::removeVarFloat(std::string varName)
{
	std::map<std::string, float >::iterator it = m_varsFloat.find(varName);
	if (it != m_varsFloat.end())
	{
		m_varsFloat.erase(varName);
		return true;
	}
	else
	{
		return false;
	}
}

bool Item::isVarFloatExist(std::string varName)
{
	std::map<std::string, float >::iterator it = m_varsFloat.find(varName);
	if (it != m_varsFloat.end())
		return true;
	else
		return false;
}

bool Item::createVarStr(std::string varName, std::string itmVar)
{
	std::map<std::string, std::string >::iterator it = m_varsStr.find(varName);
	if (it == m_varsStr.end())
	{
		setVarStr(varName, itmVar);
		return true;
	}
	else
	{
		return false;
	}
}

bool Item::removeVarStr(std::string varName)
{
	std::map<std::string, std::string >::iterator it = m_varsStr.find(varName);
	if (it != m_varsStr.end())
	{
		m_varsStr.erase(varName);
		return true;
	}
	else
	{
		return false;
	}
}

bool Item::isVarStrExist(std::string varName)
{
	std::map<std::string, std::string >::iterator it = m_varsStr.find(varName);
	if (it != m_varsStr.end())
		return true;
	else
		return false;
}

bool Item::isShown()
{
	return m_shown;
}

// -- Destructor

Item::~Item() { }