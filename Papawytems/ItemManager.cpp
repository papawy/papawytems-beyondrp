#include "ItemManager.h"

// -- Constructor

ItemManager::ItemManager() { }

// -- Destructor

ItemManager::~ItemManager() 
{ 
	for (std::map<int, Item*>::iterator it = m_itemPool.begin(); it != m_itemPool.end(); ++it)
	{
		delete it->second;
	}
}

// -- Methods

int ItemManager::addItemSchema(ItemSchema& Schema)
{
	std::map<int, ItemSchema>::iterator it = m_itemSchemaPool.end();
	if (m_itemSchemaPool.empty())
	{
		m_itemSchemaPool[0] = Schema;
		return 0;
	}
	--it;
	m_itemSchemaPool[it->first + 1] = Schema;
	return it->first + 1;
}

bool ItemManager::removeItemSchema(int id)
{
	std::map<int, ItemSchema>::iterator it = m_itemSchemaPool.find(id);
	if (it != m_itemSchemaPool.end())
	{
		m_itemSchemaPool.erase(id);
		return true;
	}
	else
	{
		return false;
	}
}

// -

int ItemManager::addItem(Item *item)
{
	std::map<int, Item*>::iterator it = m_itemPool.end();
	if (m_itemPool.empty())
	{
		m_itemPool[0] = new Item(*item);
		return 0;
	}
	--it;
	m_itemPool[it->first + 1] = new Item(*item);
	return it->first + 1;
}

int ItemManager::addItem(ItemContainer *item)
{
	std::map<int, Item*>::iterator it = m_itemPool.end();
	if (m_itemPool.empty())
	{
		m_itemPool[0] = new ItemContainer(*item);
		return 0;
	}
	--it;
	m_itemPool[it->first + 1] = new ItemContainer(*item);
	return it->first + 1;
}

bool ItemManager::removeItem(int id)
{
	std::map<int, Item*>::iterator it = m_itemPool.find(id);
	if (it != m_itemPool.end())
	{
		delete m_itemPool.find(id)->second;
		m_itemPool.erase(id);
		return true;
	}
	else
	{
		return false;
	}
}

// -

ItemSchema& ItemManager::accessItemSchema(int id)
{
	std::map<int, ItemSchema>::iterator it = m_itemSchemaPool.find(id);
	if (it != m_itemSchemaPool.end())
	{
		return m_itemSchemaPool[id];
	}
	else
	{
		throw -1;
	}
}

Item* ItemManager::accessItem(int id)
{
	std::map<int, Item*>::iterator it = m_itemPool.find(id);
	if (it != m_itemPool.end())
	{
		return m_itemPool[id];
	}
	else
	{
		throw - 1;
	}
}

bool ItemManager::itemSchemaExist(int id)
{
	if (m_itemSchemaPool.find(id) != m_itemSchemaPool.end())
		return true;
	else
		return false;
}

bool ItemManager::itemExist(int id)
{
	if (m_itemPool.find(id) != m_itemPool.end())
		return true;
	else
		return false;
}

int ItemManager::getMaxItems()
{
	return m_itemPool.size();
}

int ItemManager::getItemSchemaID(int itemID)
{
	if (m_itemPool.find(itemID) != m_itemPool.end())
	{
		for (std::map<int, ItemSchema>::iterator it = m_itemSchemaPool.begin(); it != m_itemSchemaPool.end(); ++it)
		{
			if (it->second == m_itemPool[itemID]->getSchema())
			{
				return it->first;
			}
		}
		return -1;
		
	}
	else
		return -1;
}

int ItemManager::getItemByObjectID(int objectID)
{
	for (std::map<int, Item*>::iterator it = m_itemPool.begin(); it != m_itemPool.end(); ++it)
	{
		if (it->second->getObjectID() == objectID)
		{
			return it->first;
		}
	}
	return -1;
}

int ItemManager::getItems(int &index)
{
	int count = 0;
	for(std::map<int, Item*>::iterator it = m_itemPool.begin(); it != m_itemPool.end(); ++it)
	{
		if (index == count) { index++; return it->first; }
		count++;
	}
	index = -1;
	return -1;
}