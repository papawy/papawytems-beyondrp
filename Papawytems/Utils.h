#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <string>

#include <sampgdk/core.h>
#include <sampgdk/sdk.h>

struct ObjectPosition
{
	float px;
	float py;
	float pz;

	float rx;
	float ry;
	float rz;
	ObjectPosition() { px = 0; py = 0; pz = 0; rx = 0; ry = 0; rz = 0; };
	ObjectPosition(float x, float y, float z, float rx, float ry, float rz) : px(x), py(y), pz(z), rx(rx), ry(ry), rz(rz) {};
};

bool inRange(int value, int min, int max);

std::string getPawnString(AMX* amx, cell str);

#endif // UTILS_H_INCLUDED