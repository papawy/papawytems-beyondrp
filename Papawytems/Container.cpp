#include "Container.h"

ItemContainer::ItemContainer(ItemManager* itmManagerPtr)
{
	m_itmManagerLink = itmManagerPtr;
	m_currentCapacity = 0.0;
}

ItemContainer::ItemContainer(ItemSchema& itmSchema, ItemManager* itmManagerPtr) : Item(itmSchema)
{
	m_itmManagerLink = itmManagerPtr;
	m_currentCapacity = 0.0;
}

ItemContainer::ItemContainer(ItemSchema& itmSchema, ObjectPosition position, ItemManager* itmManagerPtr) : Item(itmSchema, position)
{
	m_itmManagerLink = itmManagerPtr;
	m_currentCapacity = 0.0;
}

ItemContainer::~ItemContainer() {};



void ItemContainer::linkToItemManager(ItemManager& itmManager)
{
	m_itmManagerLink = &itmManager;
}



int ItemContainer::addItem(int itemID)
{
	if (m_itmManagerLink == nullptr)
		return -3;
	
	if (m_itmManagerLink->accessItem(itemID)->isInContainer())
		return -2;

	// Check for existing itemID in container
	if (isItemStoredIn(itemID))
		return -1;

	if (m_itmManagerLink->accessItem(itemID)->getItemSize() + m_currentCapacity > m_size)
		return 0;
	else
	{
		m_currentCapacity += m_itmManagerLink->accessItem(itemID)->getItemSize();
		m_itmManagerLink->accessItem(itemID)->setContainerState(true);
		m_container.push_back(itemID);
		return 1;
	}
}

int ItemContainer::removeItem(int itemID)
{
	if (m_itmManagerLink == nullptr)
		return -2;

	// Check for existing itemID in container
	for (std::list<int>::iterator it = m_container.begin(); it != m_container.end(); ++it)
	{
		if (*it == itemID)
		{
			m_currentCapacity -= m_itmManagerLink->accessItem(itemID)->getItemSize();
			m_itmManagerLink->accessItem(itemID)->setContainerState(false);
			m_container.erase(it);
			return 1;
		}
	}
	return 0;

}

bool ItemContainer::isItemStoredIn(int itemID)
{
	// Check for existing itemID in container
	for (std::list<int>::iterator it = m_container.begin(); it != m_container.end(); ++it)
	{
		if (*it == itemID) return true;
	}
	return false;
}

int ItemContainer::getItemsIn(int &index)
{
	int count = 0;
	for (std::list<int>::iterator it = m_container.begin(); it != m_container.end(); ++it)
	{
		if (index == count) { index++; return *it; }
		count++;
	}
	index = -1;
	return -1;
}