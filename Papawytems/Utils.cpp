#include "Utils.h"

bool inRange(int value, int min, int max)
{
	if ((value < min) || (value > max))
		return false;
	else
		return true;
}

std::string getPawnString(AMX* amx, cell str)
{
	int len = NULL,
		ret = NULL;

	cell *addr = NULL;

	amx_GetAddr(amx, str, &addr);
	amx_StrLen(addr, &len);

	if (len)
	{
		len++;

		char* text = new char[len];
		amx_GetString(text, addr, 0, len);

		std::string returnStr = text;
		delete[] text;

		return returnStr;
	}
	return "";
}